## Starting the project
1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Build the project using Maven.
4. DataBase used H2 - In memory mode.
5. Configure the database connection in the application.yml file (if needed, H2 doesn't require username and password).
7. Using the H2 Default 'PUBLIC' schema.
8. Run the application.
9. Use Postman to access the endpoints.
10. In order to see the saved data in the DB use the H2 console: http://localhost:8080/h2-console


### Files and where to find them
1. The files can be found under 'resources' package. The DDL.sql and Postman json collection with the requests.

### Project
1. Java 17
2. Maven
3. Spring Boot 3.1.2