--          Drop tables and schema
DROP TABLE IF EXISTS GAS_STATIONS;

--          Create tables
create table GAS_STATIONS
(
    ID           CHARACTER VARYING(36)  not null
        primary key
        unique,
    NAME         CHARACTER VARYING(255) not null,
    BRAND        CHARACTER VARYING(255) not null,
    STREET       CHARACTER VARYING(255) not null,
    PLACE        CHARACTER VARYING(255) not null,
    LAT          DOUBLE PRECISION       not null,
    LNG          DOUBLE PRECISION       not null,
    DIST         DOUBLE PRECISION       not null,
    DIESEL       DOUBLE PRECISION,
    E5           DOUBLE PRECISION,
    E10          DOUBLE PRECISION,
    IS_OPEN      BOOLEAN,
    HOUSE_NUMBER CHARACTER VARYING(255) not null,
    POST_CODE    INTEGER                not null
);

