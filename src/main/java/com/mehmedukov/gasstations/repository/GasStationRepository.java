package com.mehmedukov.gasstations.repository;

import com.mehmedukov.gasstations.entity.GasStation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GasStationRepository extends JpaRepository<GasStation, Long> {

  List<GasStation> findAllByNameContainingIgnoreCase(String name);
}
