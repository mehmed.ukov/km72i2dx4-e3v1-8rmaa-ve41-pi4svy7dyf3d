package com.mehmedukov.gasstations.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "gas_stations")
@JsonIgnoreProperties(ignoreUnknown = true)
public class GasStation {

  @Id
  @Column(name = "id")
  private UUID id;

  @Column(name = "name")
  private String name;
  @Column(name = "brand")
  private String brand;
  @Column(name = "street")
  private String street;
  @Column(name = "place")
  private String place;
  @Column(name = "lat")
  private double lat;
  @Column(name = "lng")
  private double lng;
  @Column(name = "dist")
  private double dist;
  @Column(name = "diesel")
  private double diesel;
  @Column(name = "e5")
  private double e5;
  @Column(name = "e10")
  private double e10;
  @Column(name = "is_open")
  @JsonProperty("isOpen")
  private boolean isOpen;
  @Column(name = "house_number")
  private String houseNumber;
  @Column(name = "post_code")
  private int postCode;

  public GasStation() {
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getPlace() {
    return place;
  }

  public void setPlace(String place) {
    this.place = place;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }

  public double getDist() {
    return dist;
  }

  public void setDist(double dist) {
    this.dist = dist;
  }

  public double getDiesel() {
    return diesel;
  }

  public void setDiesel(double diesel) {
    this.diesel = diesel;
  }

  public double getE5() {
    return e5;
  }

  public void setE5(double e5) {
    this.e5 = e5;
  }

  public double getE10() {
    return e10;
  }

  public void setE10(double e10) {
    this.e10 = e10;
  }

  public boolean isOpen() {
    return isOpen;
  }

  public void setOpen(boolean open) {
    isOpen = open;
  }

  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  public int getPostCode() {
    return postCode;
  }

  public void setPostCode(int postCode) {
    this.postCode = postCode;
  }
}
