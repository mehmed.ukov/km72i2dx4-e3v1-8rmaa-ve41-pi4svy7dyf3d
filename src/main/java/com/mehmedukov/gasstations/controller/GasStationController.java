package com.mehmedukov.gasstations.controller;

import com.mehmedukov.gasstations.service.GasStationService;
import com.mehmedukov.gasstations.entity.GasStation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static com.mehmedukov.gasstations.constants.GasStationConstants.GAS_STATIONS_DATA_IMPORTED_SUCCESSFULLY;
import static com.mehmedukov.gasstations.constants.GasStationConstants.GAS_STATIONS_JSON_URL;

@RestController
@RequestMapping("/api/gas-stations")
public class GasStationController {

  private final GasStationService gasStationService;

  @Autowired
  public GasStationController(GasStationService gasStationService) {
    this.gasStationService = gasStationService;
  }

  @PostMapping("/import-data")
  public ResponseEntity<String> importGasStationsData() throws IOException {
    gasStationService.importOpenGasStationsFromJsonUrl(GAS_STATIONS_JSON_URL);

    return ResponseEntity.ok(GAS_STATIONS_DATA_IMPORTED_SUCCESSFULLY);
  }

  @GetMapping()
  public ResponseEntity<List<GasStation>> getGasStationsByName(@RequestParam String name) {
    List<GasStation> gasStations = gasStationService.searchGasStationsByName(name);

    return ResponseEntity.ok(gasStations);
  }

  @GetMapping("/median-price")
  public ResponseEntity<Double> getMedianFuelPrice(@RequestParam String fuelType) {
    double medianFuelPrice = gasStationService.calculateMedianFuelPrice(fuelType);
    return ResponseEntity.ok(medianFuelPrice);
  }

  @GetMapping("/min-price")
  public ResponseEntity<Double> getMinFuelPrice(@RequestParam String fuelType) {
    double minFuelPrice = gasStationService.getMinFuelPrice(fuelType);
    return ResponseEntity.ok(minFuelPrice);
  }

  @GetMapping("/max-price")
  public ResponseEntity<Double> getMaxFuelPrice(@RequestParam String fuelType) {
    double maxFuelPrice = gasStationService.getMaxFuelPrice(fuelType);
    return ResponseEntity.ok(maxFuelPrice);
  }
}
