package com.mehmedukov.gasstations.service;

import com.mehmedukov.gasstations.entity.GasStation;

import java.io.IOException;
import java.util.List;

public interface GasStationService {

  void importOpenGasStationsFromJsonUrl(String jsonUrl) throws IOException;

  List<GasStation> searchGasStationsByName(String name);

  double calculateMedianFuelPrice(String fuelType);

  double getMinFuelPrice(String fuelType);

  double getMaxFuelPrice(String fuelType);
}
