package com.mehmedukov.gasstations.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mehmedukov.gasstations.entity.GasStation;
import com.mehmedukov.gasstations.repository.GasStationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static com.mehmedukov.gasstations.constants.GasStationConstants.IMPORTING_JSON_DATA_ERROR_MESSAGE;

@Service
public class GasStationServiceImpl implements GasStationService {

  private static final Logger log = LoggerFactory.getLogger(GasStationServiceImpl.class);

  private final GasStationRepository gasStationRepository;

  @Autowired
  public GasStationServiceImpl(GasStationRepository gasStationRepository) {
    this.gasStationRepository = gasStationRepository;
  }

  @Override
  public void importOpenGasStationsFromJsonUrl(String jsonUrl) throws IOException {
    URL url = new URL(jsonUrl);

    try (InputStream inputStream = url.openStream()) {
      ObjectMapper objectMapper = new ObjectMapper();
      JsonNode jsonData = objectMapper.readTree(inputStream);

      JsonNode gasStationsNode = jsonData.get("stations");

      if (gasStationsNode != null && gasStationsNode.isArray()) {
        List<GasStation> gasStations = objectMapper.readValue(gasStationsNode.toString(), new TypeReference<>() {
        });
        gasStationRepository.saveAll(gasStations);
      }
    } catch (IOException e) {
      log.error(String.format(IMPORTING_JSON_DATA_ERROR_MESSAGE, jsonUrl));
      e.printStackTrace();
    }
  }

  @Override
  public List<GasStation> searchGasStationsByName(String name) {
    return gasStationRepository.findAllByNameContainingIgnoreCase(name);
  }

  @Override
  public double calculateMedianFuelPrice(String fuelType) {
    List<Double> fuelPrices = gasStationRepository.findAll().stream()
                                                  .map(station -> getFuelPriceByType(station, fuelType))
                                                  .filter(Objects::nonNull)
                                                  .sorted()
                                                  .toList();

    int size = fuelPrices.size();

    if (size == 0) {
      return 0.0;
    }

    int middle = size / 2;

    if (size % 2 == 0) {
      return (fuelPrices.get(middle - 1) + fuelPrices.get(middle)) / 2.0;
    } else {
      return fuelPrices.get(middle);
    }
  }

  @Override
  public double getMinFuelPrice(String fuelType) {
    return gasStationRepository.findAll().stream()
                               .map(station -> getFuelPriceByType(station, fuelType))
                               .filter(price -> price != null && price > 0.0)
                               .min(Comparator.naturalOrder())
                               .orElse(0.0);
  }

  @Override
  public double getMaxFuelPrice(String fuelType) {
    return gasStationRepository.findAll().stream()
                               .map(station -> getFuelPriceByType(station, fuelType))
                               .filter(Objects::nonNull)
                               .max(Comparator.naturalOrder())
                               .orElse(0.0);
  }

  private Double getFuelPriceByType(GasStation station, String fuelType) {
    return switch (fuelType) {
      case "diesel" -> station.getDiesel();
      case "e5" -> station.getE5();
      case "e10" -> station.getE10();
      default -> null;
    };
  }
}
