package com.mehmedukov.gasstations.constants;

public final class GasStationConstants {

  GasStationConstants() {
    throw new UnsupportedOperationException();
  }

  public static final String GAS_STATIONS_JSON_URL = "https://wejago.de/assets/data/gas-stations-data.json";
  public static final String IMPORTING_JSON_DATA_ERROR_MESSAGE =
    "An error occurred while importing gas station data from the JSON URL: %s";
  public static final String GAS_STATIONS_DATA_IMPORTED_SUCCESSFULLY = "Open gas stations data imported successfully.";
}
