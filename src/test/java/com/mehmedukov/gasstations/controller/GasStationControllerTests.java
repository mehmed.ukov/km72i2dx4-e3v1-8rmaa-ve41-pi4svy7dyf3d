package com.mehmedukov.gasstations.controller;

import com.mehmedukov.gasstations.entity.GasStation;
import com.mehmedukov.gasstations.service.GasStationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.mehmedukov.gasstations.constants.GasStationConstants.GAS_STATIONS_DATA_IMPORTED_SUCCESSFULLY;
import static com.mehmedukov.gasstations.constants.GasStationConstants.GAS_STATIONS_JSON_URL;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GasStationControllerTests {

  @Mock
  private GasStationService mockGasStationService;

  @InjectMocks
  private GasStationController gasStationController;

  @Test
  public void importGasStationsData_Success() throws IOException {
    ResponseEntity<String> response = gasStationController.importGasStationsData();

    verify(mockGasStationService).importOpenGasStationsFromJsonUrl(GAS_STATIONS_JSON_URL);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(GAS_STATIONS_DATA_IMPORTED_SUCCESSFULLY, response.getBody());
  }

  @Test
  public void getGasStationsByName_Success() {
    GasStation gasStation1 = new GasStation();
    gasStation1.setName("Gas Station 1");

    GasStation gasStation2 = new GasStation();
    gasStation2.setName("Gas Station 2");

    when(mockGasStationService.searchGasStationsByName("Gas Station")).thenReturn(
      Arrays.asList(gasStation1, gasStation2));

    ResponseEntity<List<GasStation>> response = gasStationController.getGasStationsByName("Gas Station");

    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(2, response.getBody().size());
    assertEquals("Gas Station 1", response.getBody().get(0).getName());
    assertEquals("Gas Station 2", response.getBody().get(1).getName());
  }

  @Test
  public void getMedianFuelPrice_Success() {
    String fuelType = "diesel";
    double medianPrice = 2.5;

    when(mockGasStationService.calculateMedianFuelPrice(fuelType)).thenReturn(medianPrice);

    ResponseEntity<Double> response = gasStationController.getMedianFuelPrice(fuelType);

    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(Double.valueOf(medianPrice), response.getBody());
  }

  @Test
  public void getMinFuelPrice_Success() {
    String fuelType = "e5";
    double minPrice = 2.0;

    when(mockGasStationService.getMinFuelPrice(fuelType)).thenReturn(minPrice);

    ResponseEntity<Double> response = gasStationController.getMinFuelPrice(fuelType);

    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(Double.valueOf(minPrice), response.getBody());
  }

  @Test
  public void getMaxFuelPrice_Success() {
    String fuelType = "e10";
    double maxPrice = 3.5;

    when(mockGasStationService.getMaxFuelPrice(fuelType)).thenReturn(maxPrice);

    ResponseEntity<Double> response = gasStationController.getMaxFuelPrice(fuelType);

    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(Double.valueOf(maxPrice), response.getBody());
  }
}
