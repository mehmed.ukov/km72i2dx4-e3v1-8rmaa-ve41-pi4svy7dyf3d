package com.mehmedukov.gasstations.service;

import com.mehmedukov.gasstations.entity.GasStation;
import com.mehmedukov.gasstations.repository.GasStationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GasStationServiceImplTests {

  @Mock
  private GasStationRepository mockGasStationRepository;

  @InjectMocks
  private GasStationServiceImpl gasStationService;

  @Test
  public void importOpenGasStationsFromJsonUrl_ValidData() throws IOException {
    URL url = new URL("https://wejago.de/assets/data/gas-stations-data.json");

    gasStationService.importOpenGasStationsFromJsonUrl(url.toString());

    verify(mockGasStationRepository).saveAll(anyList());
  }

  @Test
  public void calculateMedianFuelPrice_EmptyList() {
    when(mockGasStationRepository.findAll()).thenReturn(List.of());

    double medianPrice = gasStationService.calculateMedianFuelPrice("diesel");

    assertEquals(0.0, medianPrice, 0.001);
  }

  @Test
  public void calculateMedianFuelPrice_OddSizeList() {
    GasStation station1 = new GasStation();
    station1.setDiesel(5.0);

    GasStation station2 = new GasStation();
    station2.setDiesel(6.0);

    GasStation station3 = new GasStation();
    station3.setDiesel(4.0);

    when(mockGasStationRepository.findAll()).thenReturn(Arrays.asList(station1, station2, station3));

    double medianPrice = gasStationService.calculateMedianFuelPrice("diesel");

    assertEquals(5.0, medianPrice, 0.001);
  }

  @Test
  public void calculateMedianFuelPrice_EvenSizeList() {
    GasStation station1 = new GasStation();
    station1.setDiesel(5.0);

    GasStation station2 = new GasStation();
    station2.setDiesel(6.0);

    GasStation station3 = new GasStation();
    station3.setDiesel(4.0);

    GasStation station4 = new GasStation();
    station4.setDiesel(8.0);

    when(mockGasStationRepository.findAll()).thenReturn(Arrays.asList(station1, station2, station3, station4));

    double medianPrice = gasStationService.calculateMedianFuelPrice("diesel");

    assertEquals(5.5, medianPrice, 0.001);
  }

  @Test
  public void getMinFuelPrice_NoValidPrices() {
    when(mockGasStationRepository.findAll()).thenReturn(List.of());

    double minPrice = gasStationService.getMinFuelPrice("e5");

    assertEquals(0.0, minPrice, 0.001);
  }

  @Test
  public void getMinFuelPrice_ValidPrices() {

    GasStation station1 = new GasStation();
    station1.setE5(5.0);

    GasStation station2 = new GasStation();
    station2.setE5(6.0);

    GasStation station3 = new GasStation();
    station3.setE5(4.0);

    when(mockGasStationRepository.findAll()).thenReturn(Arrays.asList(station1, station2, station3));

    double minPrice = gasStationService.getMinFuelPrice("e5");

    assertEquals(4.0, minPrice, 0.001);
  }

  @Test
  public void getMinFuelPrice_ValidAndInvalidPrices() {
    GasStation station1 = new GasStation();
    station1.setE5(5.0);

    GasStation station2 = new GasStation();
    station2.setE5(-1.0);

    GasStation station3 = new GasStation();
    station3.setE5(4.0);

    when(mockGasStationRepository.findAll()).thenReturn(Arrays.asList(station1, station2, station3));

    double minPrice = gasStationService.getMinFuelPrice("e5");

    assertEquals(4.0, minPrice, 0.001);
  }

  @Test
  public void testGetMaxFuelPrice_NoValidPrices() {
    when(mockGasStationRepository.findAll()).thenReturn(List.of());

    double maxPrice = gasStationService.getMaxFuelPrice("e10");

    assertEquals(0.0, maxPrice, 0.001);
  }

  @Test
  public void testGetMaxFuelPrice_ValidPrices() {
    GasStation station1 = new GasStation();
    station1.setE10(3.0);

    GasStation station2 = new GasStation();
    station2.setE10(2.5);

    GasStation station3 = new GasStation();
    station3.setE10(2.8);

    when(mockGasStationRepository.findAll()).thenReturn(Arrays.asList(station1, station2, station3));

    double maxPrice = gasStationService.getMaxFuelPrice("e10");

    assertEquals(3.0, maxPrice, 0.001);
  }

  @Test
  public void testGetMaxFuelPrice_ValidAndInvalidPrices() {
    GasStation station1 = new GasStation();
    station1.setE10(3.0);

    GasStation station2 = new GasStation();
    station2.setE10(-1.0);

    GasStation station3 = new GasStation();
    station3.setE10(2.8);

    when(mockGasStationRepository.findAll()).thenReturn(Arrays.asList(station1, station2, station3));

    double maxPrice = gasStationService.getMaxFuelPrice("e10");

    assertEquals(3.0, maxPrice, 0.001);
  }
}
